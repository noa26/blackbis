"""
Black Biss - Basic Python

Write a function that replace any second word with the word "Corona".
"""


def word_pandemic(in_array):
    """
    Function that replaces every other word with the word "Corona".
	
	Must add [""] to splitted array so that sentences of even (not odd) length 
	(like: "Koala Bears are the") will work.
    """
    return " Corona ".join((in_array.split() + [""])[::2]).strip()


# small test to check it's work.
if __name__ == '__main__':
    base_word = "Koala Bears are the cutest"
    sick_word = "Koala Corona are Corona cutest"

    if word_pandemic(base_word) == sick_word:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
