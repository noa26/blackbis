import sys
from collections import Counter


def print_words(filename):
    """
    Prints all words and their counts in lexicographic order. 
    """
    words_counter = count_words(filename)

    # convert to dictionay. dictionary keys are already lexicographically ordered. 
    for key in sorted(dict(words_counter).keys()):
        print(key, words_counter[key])


def print_top(filename):
    """
    Prints the 20 most common words.
    """
    words_counter = count_words(filename)
    for value, count in words_counter.most_common(20):
        print(value, count)


def count_words(filename):
    """
    Recieves a file name and returns a counter of all of its words.
    Strips the words from common punctuation marks (uses translation for better performance).
    """
    # create a map that maps every punctuation mark to a space.
    marks = '_-.,?!()[]:;`*\"'
    table = str.maketrans(marks, " " * len(marks))

    with open(filename, 'r') as file:
        data = file.read().lower()

        # 'translate' punctuation marks to spaces and split by space.
        c = Counter(data.translate(table).split())

        del c[" "]
        del c["\'"]
        return c


def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | ---topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()
