def camel_case(in_str):
    """
    Separate words written in camelCase.
    """
    i = 0
    words = []
    for j in range(len(in_str)):
        if in_str[j].isupper() and j != i:
            words.append(in_str[i:j])
            i = j
    words.append(in_str[i:])
    return " ".join(words)


if __name__ == "__main__":
    print(camel_case("camelCaseIsAwesome"))
