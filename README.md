# BlackBiss
BlackBiss assignments.

## GIT
- Learn GIT Branching

## Python - Basics
- [odd_ones_out.py](python_basics/odd_ones_out/odd_ones_out.py)
- [word_pandemic.py](python_basics/word_pandemic/word_pandemic.py)
- [word_count.py](python_basics/word_count/word_count.py)
- [camel_case.py](python_basics/camel_case/camel_case.py)

## Python - Advanced
- [home_economic.py](python_advanced/home_economics/home_economics.py)
- [ejected.py](python_advanced/ejected/ejected.py)
- [dejumble.py](python_advanced/pirates_of_the_biss/dejumble.py)
- [password_check.py](python_advanced/dance_of_regex/password_check.py)
- [delivery.py](python_advanced/delivery/delivery.py)
- [crypto_breaker.py](python_advanced/crypto_breaker/crypto_breaker.py)

## C - Basics
- [fibonacci.c](c_basics/fibonacci/fibonacci.c)
- [substrings.c](c_basics/substrings/substrings.c)
- [word_count.c](c_basics/word_count/word_count.c)
- [getline.c](c_basics/getline/getline.c)
- [linked_list.c](c_basics/linked_lists_are_fun/linked_list.c)
- [natans_apartment.c](c_basics/natans_apartment/natans_apartment.c) (compilation: `gcc natans_apartment.c linked_list.c -o main`)

## C - Advanced
- [pokemon_go.c](c_advanced/pokemon_go/pokemon_go.c)
- [rec2sqr.c](c_advanced/rec2sqr/rec2sqr.c)
- [tower_of_the_biss.c](c_advanced/tower_of_the_biss/tower_of_the_biss.c)
- [brainf.c](c_advanced/brainf/brainf.c)
