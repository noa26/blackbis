/**
 * @file pokemon_go.c
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief Pokemon-go! struct.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

 // ------------------------------ enums ------------------------------
typedef enum
{
    Water, Fire, Earth, Air
} Element;

 // ------------------------------ structs ------------------------------
typedef struct pokemon 
{
    int attack;
    int defence;
    Element element;
    char* name;
} Pokemon;


// ------------------------------ functions -----------------------------
int effectiveness(Element player, Element oponent) 
{
    switch (player) 
    {
    case Water:
        switch (oponent)
        {
        case Fire:
            return 3;
        case Earth:
            return 1;
        default:
            return 2;
        }
    case Fire:
        switch (oponent)
        {
        case Air:
            return 3;
        case Water:
            return 1;
        default:
            return 2;
        }
    case Earth:
        switch (oponent)
        {
        case Water:
            return 3;
        case Air:
            return 1;
        default:
            return 2;
        }
    case Air:
        switch (oponent)
        {
        case Earth:
            return 3;
        case Fire:
            return 1;
        default:
            return 2;
        }
    }
    // would never reach here.
    return -1;
}

void print_pokemon_id(Pokemon * p) 
{
    printf("%s\n", p->name);
    printf("\tattack: %d\n", p->attack);
    printf("\tdefence: %d\n", p->defence);
    printf("\teffectiveness: %d\n", p->element);
}

int strength(int attack, int defence, Element attack_element, Element defence_element)
{
    return (attack / defence) * effectiveness(attack_element, defence_element) * 50;

}

void dual(Pokemon* p1, Pokemon* p2)
{
    if (!p1 || !p2) {
        return;
    }

    int p1_strength = strength(p1->attack, p2->defence, p1->element, p2->element);
    int p2_strength = strength(p2->attack, p1->defence, p2->element, p1->element);

    if (p1_strength > p2_strength)
    {
        printf("%s Won!\n", p1->name);
    }
    else if (p1_strength < p2_strength)
    {
        printf("%s Won!\n", p2->name);
    }
    else
    {
        printf("Tie!");
    }

}

void init_pokemon(Pokemon* p, int attack, int defence, Element element, const char* name) 
{
    p->attack = attack;
    p->defence = defence;
    p->element = element;
    p->name = name;
}


int main(int argc, char* argv[])
{
    Pokemon john;
    Pokemon doe;
    
    init_pokemon(&john, 1, 1, Air, "John");
    init_pokemon(&doe, 1, 1, Water, "Doe");

    print_pokemon_id(&john);
    print_pokemon_id(&doe);

    dual(&john, &doe);
    return 0;
}
