#define _CRT_SECURE_NO_WARNINGS
/**
 * @file brainf.c
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief Tower Of The Biss.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>

// ------------------------------ functions -----------------------------
void interperet_bf(FILE * stream, FILE * output, char* memory) 
{
    // c: command character.
    // ptr: memory pointer.
    // indent_level: indentation level.
    char c;
    char* ptr = memory;
    size_t indent_level = 0;

    while ((c = fgetc(stream)) != EOF)
    {
        switch (c)
        {
        case '>':
            ++ptr;
            break;
        case '<':
            --ptr;
            break;
        case '+':
            *ptr += 1;
            break;
        case '-':
            *ptr -= 1;
            break;
        case '.':
            fprintf(output, "%c", *ptr);
            break;
        case '[':
            if (*ptr == 0) {
                // skip forward untill reached ']'.
                char c_dummy = ']';
                size_t indent = 1;
                
                do {
                    // the skip:
                    c_dummy = fgetc(stream);
                    if (c_dummy == '[') 
                    {
                        ++indent;
                    }
                    else if (c_dummy == ']') 
                    {
                        --indent;
                    }
                    else if (c_dummy == EOF) 
                    {
                        printf("Invalid file!\nCould not find \']\'.\nShutting down.\n");
                        fclose(stream);
                        exit(1);
                    }
                } while (indent > 0);
                // reached ']'. will reach command in next iteration.
            }
            break;
        case ']':
            if (*ptr != 0) {
                // skip backwards untill reached '['.
                char c_dummy = '[';
                size_t indent = 1;

                do {
                    if (fseek(stream, -2L, SEEK_CUR) == 0) {
                        c_dummy = fgetc(stream);
                        if (c_dummy == ']') {
                            ++indent;
                        }
                        else if (c_dummy == '[') {
                            --indent;
                        }
                    }
                    else {
                        printf("Invalid file!\nCould not find \'[\'.\nShutting down.\n");
                        fclose(stream);
                        exit(1);
                    }
                } while (indent > 0);
                // reached '['. will reach command in next iteration.
            }
            break;
        default:
            break;
        }
    }

}


int main(int argc, char* argv[])
{
    // open input stream
    FILE* in = fopen("program.txt", "r");
    char mem[1000];

    // initialize memory.
    for (int i = 0; i < 1000; ++i) {
        mem[i] = (char)0;
    }

    interperet_bf(in, stdout, mem);

    return 0;
}
