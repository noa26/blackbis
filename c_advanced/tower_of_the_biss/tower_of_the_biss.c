/**
 * @file tower_of_the_biss.c
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief Tower Of The Biss.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ includes ------------------------------
#include <stdio.h>

 // ------------------------------ enums ------------------------------
typedef enum{A = (int)'A', B, C} Pole;

// ------------------------------ functions -----------------------------
void transfer_disks(size_t n, Pole origin, Pole mid, Pole destination, int should_print)
{
    if (n == 1) 
    {
        if (should_print){
            // transfer to destination pole
            printf("Move disk %d from %c to %c\n", n, origin, destination);
        }
    }
    else 
    {
        // move all disks but last to mid-pole
        transfer_disks(n - 1, origin, destination, mid, 1);

        // transfer last to destination pole
        transfer_disks(1, origin, mid, destination, 0);
        printf("Move disk %d from %c to %c\n", n, origin, destination);

        // transfer all disks on mid to to destination
        transfer_disks(n - 1, mid, origin, destination, 1);
    }
}

int main(int argc, char* argv[])
{
    transfer_disks(3, A, B, C, 1);
    return 0;
}
