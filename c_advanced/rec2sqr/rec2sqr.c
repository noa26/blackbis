/**
 * @file rec2sqr.c
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief rec2sqr.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ macros ------------------------------
#define MIN(x, y) ((x<y)?x:y)

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

 // ------------------------------ structs ------------------------------
typedef struct rectangle {
    size_t height;
    size_t width;
} Rectangle;

typedef struct squares_array {
    size_t length;
    size_t* squares;
} sqr_array;

// ------------------------------ functions -----------------------------
/// <summary>
/// rec2sqr: calculates the minimum amount of squares that can fill the rectangle.
/// </summary>
/// <param name="rect">rectangle to be filled.</param>
/// <returns>an array of the squares dimensions.</returns>
sqr_array rec2sqr(Rectangle * rect)
{
    if (!rect) 
    {
        sqr_array a = { .squares = NULL, .length = 0 };
        return a;
    }

    Rectangle remainder = {.height = rect->height, .width = rect->width};
    size_t count = 0;
    
    // maximum amount of squares = height * width.
    size_t* squares = (size_t*)malloc(sizeof(size_t) * rect->height * rect->width);
    if (!squares) {
        printf("memory allocation error at: rec2sqr.\n");
        exit(1);
    }

    while (remainder.height > 0 && remainder.width > 0)
    {
        // find minimum edge.
        int edge = MIN(remainder.height, remainder.width);
        
        // add to squares array.
        *(squares + count) = edge;
        ++count;

        // fix remainder.
        if (edge == remainder.height) 
        {
            remainder.width -= edge;
        }
        else 
        {
            remainder.height -= edge;
        }
    }

    // re-allocate squares array less wastefully.
    size_t* arr = (size_t*)malloc(sizeof(size_t) * count);
    if (!arr) {
        printf("memory allocation error at: rec2sqr.\n");
        exit(1);
    }
    memcpy(arr, squares, count * sizeof(size_t));

    // free wasted space.
    free(squares);

    sqr_array s = { .length = count, .squares = arr };
    /*
    * note:
    *   returning the struct itself (rather than a pointer to one on the heap)
    *   only copies a pointer and an integer, therby, not a wastful as you may think.
    */
    return s;
}

void print_sqr_array(sqr_array arr)
{
    for (int i = 0; i < arr.length; ++i)
    {
        printf("%d, ", (arr.squares)[i]);
    }
}

void clear_sqr_array(sqr_array arr) {
    free(arr.squares);
}

int main(int argc, char* argv[])
{
    // create a rectangle.
    Rectangle rect = {.height = 5, .width = 3};

    // check how many squares can be inserted.
    sqr_array squares = rec2sqr(&rect);
    
    // print squares
    print_sqr_array(squares);
    return 0;
}
