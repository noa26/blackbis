/**
 * @file linked_list.c
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief program that creates a linked list, prints it, and releases it.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

 // ------------------------------ includes ------------------------------
#define NULL 0

 // ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

 // ------------------------------ structs ------------------------------
typedef struct node {
    int value;
    struct node * next;
} Node;


// ------------------------------ functions -----------------------------

/*
 * Function:  create
 * --------------------
 * Creates a new linked list.
 *
 *    value:    the first node's value.
 *    returns: the new linked-list's head pointer.
 */
Node* create_list(int v)
{
    Node* head = (Node*)malloc(sizeof(Node));
    if (head != NULL)
    {
        (*head).value = v;
        head->next = NULL;
        return head;
    }
    else
    {
        return (Node*)NULL;
    }
}


/*
 * Function:  insert
 * --------------------
 * Inserts a new value into the linked list.
 * If index too big, adds to end.
 * If head it NULL, returns NULL.
 * 
 *    head:     the linked list's head.
 *    value:    the value to insert.
 *    i:        the index of insertion.
 *    returns: the new linked-list's head pointer.
 */
Node * insert(Node * head, int value, int i) 
{
    if (!head) {
        return head;
    }
    
    Node * curr = NULL;
    Node * next = head;

    // first reach the index
    for (int j = 0; j < i && next != NULL; ++j)
    {
        curr = next;
        next = next->next;
    }

    // create a node
    Node* n = create_list(value);
    if (n == NULL) {
        // couldn't create a node. No addition happens.
        return head;
    }

    // then add between curr and next
    if (curr) 
    {
        curr->next = n;
        n->next = next;
    }
    else 
    {
        head = n;
        n->next = next;
    }

    return head;
}


/*
 * Function:  print_list
 * --------------------
 * prints the linked list.
 *
 *    head:    the linked list's head (first node).
 */
void print_list(Node* head)
{
    Node* curr = head;
    while (curr != NULL)
    {
        printf("value: %d\t next: %p\n", curr->value, curr->next);
        curr = curr->next;
    }
}


/*
 * Function:  free_list
 * --------------------
 * frees the linked list.
 *
 *    head:    the linked list's head (first node).
 */
void free_list(Node * head)
{
    Node * curr = head;
    Node * next = NULL;
    while (curr != NULL)
    {
        next = curr->next;
        free(curr);
        curr = next;
    }
}


int main(int argc, char* argv[])
{
    Node* n = create_list(3);
    n = insert(n, 1, 0);
    n = insert(n, 2, 1);
    n = insert(n, 4, 3);
    n = insert(n, 5, 4);
    
    print_list(n);
    free_list(n);

    return 0;
}
