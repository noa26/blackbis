/**
 * @file getline.c
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief program that read lines from FILE (file or stdin).
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

 // ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>

// ------------------------------ functions -----------------------------

// returns if a character represents end of line.
int endline(char c)
{
    char delim[] = "\n\r";
    for (int i = 0; i <= 2; ++i) {
        if (c == delim[i]) {
            return 1;
        }
    }

    // returns true if reached EOF!
    return (int)c <= 0;
}


size_t my_getline(char** lineptr, size_t* n, FILE* stream)
{
    // Read up to (and including) a newline from STREAM into *lineptr.
    // Max characters to be read: n
    char c;
    size_t i = 0;

    while (i < *n - 2) {
        c = fgetc(stream);
        if (endline(c)) {
            break;
        }
        (*lineptr)[i] = c;
        ++i;
    }

    (*lineptr)[i] = c;
    (*lineptr)[i + 1] = '\0';
    return ++i;
}

int main()
{
    char* buffer;
    size_t bufsize = 32;
    size_t characters;

    buffer = (char*)malloc(bufsize * sizeof(char));
    if (buffer == NULL)
    {
        perror("Unable to allocate buffer");
        exit(1);
    }

    printf("Type something: ");
    characters = my_getline(&buffer, &bufsize, stdin);
    printf("%zu characters were read.\n", characters);
    printf("You typed: '%s'\n", buffer);

    return(0);
}