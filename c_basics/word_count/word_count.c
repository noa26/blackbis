/**
 * @file word_count.c
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief program that print how many words are in a given file.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 * @section DESCRIPTION
 * The system get file path as argument. Open it and count the word's in it.
 * Input  : file_path
 * Output : print out the amount of words.
 */

#define _CRT_SECURE_NO_WARNINGS

 // ------------------------------ includes ------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ------------------------------ functions -----------------------------
// declarations
int is_alpha(char c);
int is_seperator(char c);


// implementations
int main(int argc, char* argv[])
{
    size_t word_count = 0;
    // you can add more initial checks
    if (argc < 2)
    {
        printf("missing file_path parameter.\n");
        return EXIT_FAILURE;
    }

    // open file stream
    FILE* stream = fopen(argv[1], "r");
    if (!stream) 
    {
        printf("unable to open file.\n");
        return EXIT_FAILURE;
    }

    // c: temporary iterator. 
    // letter_index: index of the letter in the word (-1 if not in a word).
    char c = '\0';
    int letter_index = -1;

    // iterate through file. char by char.
    do {
        c = (char)fgetc(stream);
        if (letter_index == -1) {
            if (is_alpha(c)) {
                // start of a word
                ++letter_index;
            }
        }
        else {
            // we're inside a word
            if (is_seperator(c)) {
                ++word_count;
                letter_index = -1;
            }
            else {
                ++letter_index;
            }
        }
    } while (c != EOF && word_count + 1 > word_count);

    // close file stream
    fclose(stream);

    printf("The file %s contain %u words.\n", argv[1], word_count);

    return 0;
}


// returns 1 if 'c' is a letter. 0 otherwise.
int is_alpha(char c)
{
    return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
}

// returns 1 if 'c' is a word seperator. 0 otherwise.
int is_seperator(char c)
{
    char delimiters[] = " -.`_\n\r\0";

    int i;
    for (i = 0; i < strlen(delimiters); ++i) {
        if (c == delimiters[i]) {
            return 1;
        }
    }
    return (c == EOF);
}
