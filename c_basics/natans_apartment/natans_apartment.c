/**
 * @file nathans_apartment.c
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief naive code generation.
 * Uses a node class similar to the implementation in previous exercises for easier memory manipulation.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

// ------------------------------ defines ------------------------------
#define _CRT_SECURE_NO_WARNINGS

// ------------------------------ includes ------------------------------
#include <stdio.h>
#include "linked_list.h"

// ------------------------------ globals ------------------------------
char intercom[4][3] = { {'1', '2', '3'},
                        {'4', '5', '6'},
                        {'7', '8', '9'},
                        {'\0','0', '\0'} };
Node* list = NULL;

// ------------------------------ functions -----------------------------
void calc_options(char digit, char dest[5]) 
{
    int i = 0, j = 0, index = 0;
    int found = 0;
    // first find location.
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 3 && !found; j++) {
            if (intercom[i][j] == digit) {
                found = 1;
                break;
            }
        }
        if (found)
            break;
    }
    
    // now add all options.
    if ((i + 1) < 3 && intercom[i + 1][j] != '\0') {
        dest[index] = intercom[i + 1][j];
        ++index;
    }
    if ((i - 1) >= 0 && intercom[i - 1][j] != '\0') {
        dest[index] = intercom[i -1][j];
        ++index;
    }

    if ((j + 1) < 3 && intercom[i][j + 1] != '\0') {
        dest[index] = intercom[i][j + 1];
        ++index;
    }

    if ((j - 1) >= 0 && intercom[i][j - 1] != '\0') {
        dest[index] = intercom[i][j - 1];
        ++index;
    }

    dest[index] = intercom[i][j];
    return;
}

void append_c(char* s, char c) {
    // reach last digit
    size_t i = strlen(s);

    // add c to the end of the string
    s[i] = c;
    s[i + 1] = '\0';
}

void cut_c(char* s) {
    // reach last digit
    size_t i = strlen(s);
    s[i - 1] = '\0';
}


char* get_sequence(char* prefix, char* suffix, size_t n, size_t m) {
    //printf("prefix: %s suffix: %s\n", prefix, suffix);

    if (m == 0) {

        // add to list
        list = push(list, prefix);
        return NULL;
    }

    char options[5] = { 0, 0, 0, 0, 0 };
    calc_options(suffix[0], options);

    for (int i = 0; i < 5; ++i) {
        if (options[i] != 0) {
            append_c(prefix, options[i]);
            get_sequence(prefix, suffix + 1, n + 1, m - 1);
            cut_c(prefix);
        }
    }
    //  return the prefix
    return NULL;
}

int main(int argc, char* argv[])
{
    char* bad_code = "1357";
    char c[5] = "";

    get_sequence(c, bad_code, 0, 4);
    print_list(list);
    free_list(list);

    return 0;
}
