/**
 * @file linked_list.h
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief linked list c implementation.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

 // ------------------------------ defines -----------------------------
#define _CRT_SECURE_NO_WARNINGS

 // ------------------------------ includes -----------------------------
#include <stdio.h>
#include "linked_list.h"

// ------------------------------ functions -----------------------------

Node* create_list(const char* val)
{
    Node* head = (Node*)malloc(sizeof(Node));
    char* value_cpy = malloc(sizeof(char) * (strlen(val) + 1));
    strncpy(value_cpy, val, strlen(val) + 1);

    if (head != NULL)
    {
        head->value = value_cpy;
        head->next = NULL;
        return head;
    }
    else
    {
        printf("memory allocation failed.\nExiting program.\n");
        exit(1);
    }
    return NULL;
}

Node* push(Node* head, const char* value) {
    Node* n = create_list(value);
    n->next = head;
}

void print_list(Node* head)
{
    Node* curr = head;
    while (curr != NULL)
    {
        printf("%s\n", curr->value);
        curr = curr->next;
    }
}

void free_list(Node* head)
{
    Node* curr = head;
    Node* next = NULL;
    while (curr != NULL)
    {
        next = curr->next;
        free(curr->value);
        free(curr);
        curr = next;
    }
}