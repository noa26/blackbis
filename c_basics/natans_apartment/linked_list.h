/**
 * @file linked_list.h
 * @version 1.0
 * @author Noa Madmon
 *
 * @brief linked list c header.
 *
 * @section LICENSE
 * This program is part of the Black-Biss;
 * ANY USE OF THE PROGRAM OR THE SOURCE CODE WITHOUT THE BLACK-BISS AGREEMENT IS FORBIDDEN.
 *
 */

 // ------------------------------ includes ------------------------------
#include <string.h>
#include <stdlib.h>

 // ------------------------------ structs ------------------------------
typedef struct node {
    char* value;
    struct node* next;
} Node;


// ------------------------------ functions -----------------------------

/*
 * Function:  create
 * --------------------
 * Creates a new linked list.
 *
 *    value:    the first node's value.
 *    returns: the new linked-list's head pointer.
 */
Node* create_list(char* val);


/*
 * Function:  push
 * --------------------
 * Pushes a new value into the linked list.
 * If head it NULL, creates a new list.
 *
 *    head:     the linked list's head.
 *    value:    the value to insert.
 *    returns: the new linked-list's head pointer.
 */
Node* push(Node* head, const char* value);

/*
 * Function:  print_list
 * --------------------
 * prints the linked list.
 *
 *    head:    the linked list's head (first node).
 */
void print_list(Node* head);


/*
 * Function:  free_list
 * --------------------
 * frees the linked list.
 *
 *    head:    the linked list's head (first node).
 */
void free_list(Node* head);