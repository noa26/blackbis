"""
Black Biss - Advance Python

Write a function that get array of string, each represent a row in 2-dim map, when:
    'X' is start / destination point
    '-' is road right-left
    '|' is road up-down
    '+' is a turn in the road, can be any turn (which isn't continue straight)
    path = ["           ",
            "X-----+    "
            "      |    "
            "      +---X",
            "           "]

    # Note: this grid is only valid when starting on the right-hand X, but still considered valid
    path = ["                      ",
            "   +-------+          ",
            "   |      +++---+     ",
            "X--+      +-+   X     "]
    nevigate_validate(path)  # ---> True
"""


def nevigate_validate(path):
    # find 'X' positions.
    src1, src2 = None, None
    for i in range(len(path)):
        for (j, op) in enumerate(path[i]):
            if op == 'X':
                if src1 is None:
                    src1 = (i, j)
                elif src2 is None:
                    src2 = (i, j)
                else:
                    # got 3 'X'.
                    return False
    return validate(src1, src2, path) or validate(src2, src1, path)


def validate(start, end, path):
    prior = None
    curr = start
    visited = set()
    while curr != end:
        if curr in visited:
            return False
        else:
            visited.add(curr)
        options = moves(curr, prior, path)
        if len(options) != 1:
            return False
        else:
            prior = curr
            curr = options.pop()

    # look for floating characters.
    # number of points in path:
    num = len("".join(path)) - "".join(path).count(" ")
    if len(visited) + 1 != num:
        return False

    return True


def moves(location, prev_location, path) -> set:
    x, y = location
    curr = path[x][y]

    direction = None
    if prev_location:
        if prev_location[0] != x:
            direction = '|'
        else:
            direction = '-'

    # calculate neighborhoods first.
    neighborhood = get_neighborhood(location, path, direction=direction)
    vertical_n = get_neighborhood(location, path, direction='|')
    horizontal_n = get_neighborhood(location, path, direction='-')

    options = set()
    if curr == 'X':
        for (i, j) in neighborhood:
            if path[i][j] in ['X', '+']:
                options.add((i, j))
            elif path[i][j] == '|' and direction != '-' and (i, j) in vertical_n:
                options.add((i, j))
            elif path[i][j] == '-' and direction != '|' and (i, j) in horizontal_n:
                options.add((i, j))
        return options

    elif curr == '+':
        if direction:
            direction = direction.translate(str.maketrans('-|', '|-'))
        for (i, j) in get_neighborhood(location, path, direction=direction):
            if path[i][j] in ['X', "+"]:
                options.add((i, j))
            elif path[i][j] == '|' and direction == '|':
                options.add((i, j))
            elif path[i][j] == '-' and direction == '-':
                options.add((i, j))

        return options - {prev_location}

    elif curr == '|':
        if direction == '|':
            return vertical_n - {prev_location}

    elif curr == '-':
        return horizontal_n - {prev_location}
    return set()


def get_neighborhood(location, grid, direction=None):
    x, y = location
    n, m = len(grid), len(grid[0])
    neighborhood = set()

    for i in [-1, 1]:
        if i + x in range(n) and direction != '-':
            neighborhood.add((x + i, y))
        if y + i in range(m) and direction != '|':
            neighborhood.add((x, y + i))
    return neighborhood


# small test to check it's work.
if __name__ == '__main__':
    path = ["           ",
            "X-----+    ",
            "      |    ",
            "      +---X",
            "           "]
    valid = nevigate_validate(path)

    path = ["           ",
            "X--|--+    ",
            "      -    ",
            "      +---X",
            "           "]
    invalid = nevigate_validate(path)
    if valid and not invalid:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")