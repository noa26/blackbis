"""
Black Biss - Advance Python

Write a function that calculate how far is our ejected pilot.
Run this program and insert the following lines (each end with Enter):

UP 5
DOWN 3
LEFT 3
RIGHT 2
0

and the result should be 2
"""

from math import sqrt

add = lambda a, b: (a[0] + b[0], a[1] + b[1])
mult = lambda s, t: (s * t[0], s * t[1])


def ejected():
    directions = {"UP": (0, 1), "DOWN": (0, -1), "RIGHT": (1, 0), "LEFT": (-1, 0)}
    location = (0, 0)

    while True:
        action = input()
        if action.strip() == "0":
            break

        assert (len(action.split()) == 2)
        d, steps = action.split()
        assert (steps.isdigit())

        # apply move. if (a, b) is initial state and action is "UP 3", calculation is:
        # (a, b) = (a, b) + 3 * (0, 1)
        location = add(location, mult(int(steps), directions[d]))
    return int(round(sqrt(location[0] ** 2 + location[1] ** 2)))


# small test to check it's work.
if __name__ == '__main__':
    print(ejected())
