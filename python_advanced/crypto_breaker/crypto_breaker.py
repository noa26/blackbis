"""
Black Biss - Advance Python

Write a function that get a path to file that encrypted with 3 english-small-letter xor key, and print the
deciphered text.
"""

from collections import Counter


def crypto_breaker(file_path):
    # load data from file
    with open('big_secret.txt', 'r') as f:
        cipher = [int(c) for c in f.read().split(',')]

    # find commonness of characters in data.
    chr_counters = [Counter(cipher[::3]).most_common(1)[0][0],
                    Counter(cipher[1::3]).most_common(1)[0][0],
                    Counter(cipher[2::3]).most_common(1)[0][0]]

    key = []
    # the most common character in english text is commonly " ". (the space character).
    # look for characters that their xor with most common generates " ".
    for i in range(len(chr_counters)):
        for c in range(ord('a'), ord('z') + 1):
            if chr(c ^ chr_counters[i]) == " ":
                key.append(c)

    # multiply key and decrypt.
    key = key * int((len(cipher) / 3))
    print(''.join([chr(key[i] ^ cipher[i]) for i in range(len(cipher))]))


# small test to check it's work.
if __name__ == '__main__':
    crypto_breaker('big_secret.txt')
