"""
Black Biss - Advance Python

Write a function that calculate how many month one should save money to buy new car.
"""


def home_economics(startPriceOld, startPriceNew, savingPerMonth, percentLossByMonth):
    # write your code here
    old_price = startPriceOld
    new_price = startPriceNew
    loss_rate = percentLossByMonth
    savings = 0

    for i in range(int((startPriceNew / savingPerMonth)) + 1):

        # check if can buy
        if savings + old_price > new_price:
            return i, round(savings + old_price - new_price)

        # update prices and rates
        savings += savingPerMonth
        print(loss_rate)
        old_price -= (old_price * loss_rate) / 100
        new_price -= (new_price * loss_rate) / 100
        if i % 2 == 0:
            loss_rate += 0.5


# small test to check it's work.
if __name__ == '__main__':

    ret = home_economics(2000, 8000, 1000, 1.5)
    if ret[0] == 6 and ret[1] == 766:
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
