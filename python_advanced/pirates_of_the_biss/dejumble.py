"""
Black Biss - Advance Python

Write a function that suggest which word is the Pirates meant to write.
"""

from collections import Counter


def dejumble(word, potentional_words):
    # collections.Counter counts the frequency for each character in a string.
    # two Counters are equal iff every character's frequency is equal.

    jumble = Counter(word)
    return [valid for valid in potentional_words if Counter(valid) == jumble]


# small test to check it's work.
if __name__ == '__main__':
    ret = dejumble("orspt", ["sport", "parrot", "ports", "matey"])
    if len(ret) == 2 and set(ret) == set(["sport", "ports"]):
        print("HAZA! simple test pass")
    else:
        print("Oops, it's not working yet")
