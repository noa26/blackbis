"""
Black Biss - Advance Python

Write a function that read one line from the user with many potentional passwords seperated by comma ","
and print out only the valid password - by the following rules:
    * contain letter from each of the characters-sets:
        lower-case english, upper-case english, decimal digit,
        special characters *&@#$%^
    * dosn't contain other symbols (ignore white-spaces)
    * length between 6 and 12

"""
import re


def validate_passwords():
    # # receive input from file
    # with open("input_passwords.txt", "r") as f:
    #     passwords = f.read().split(",")

    # receive input via command line
    passwords = input().split(",")

    # disregard spaces
    passwords = [p.strip() for p in passwords]
    # print all valid passwords
    print([p for p in passwords if is_valid(p) == True])


def is_valid(password):
    # DOES NOT CHECK ILLEGAL CHARACTERS.

    # overview: lowercase, uppercase, digit, special character, length limit.
    conditions = ["[a-z]+", "[A-Z]+", "[0-9]+", "[*&@#$%^]+", "^.{6,12}$"]
    for regex in conditions:
        x = re.search(regex, password)
        if x is None:
            return False
    return True


# small test to check it's work.
if __name__ == '__main__':
    validate_passwords()
